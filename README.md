# README #
Simple Logger with Go

## Installation
```
go get bitbucket.org/Aishee/gologger
```

## Testing
```
go test bitbucket.org/Aishee/gologger
```

## Usage

### Logger
```go
type Logger struct {
  Level string
  Colorize bool
  Debug bool
  Exit bool
  Stack int
}
```